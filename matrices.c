//Samantha Roldán García
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "edth.h"

void iniEmptyMatrix(int files, int columns, char matrix[][15]);
void printMatrix(int files, int columns, char matrix[][15]);
void insertCharOnMatrix(int files, int columns, char matrix[][15], int file, int column, char c);
void deleteCharOnMatrix(int files, int columns, char matrix[][15], int file, int column);

int main(){}

