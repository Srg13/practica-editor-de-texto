//Samantha Roldán García
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "edth.h"

void setActiveWindow(int appData[], int aw);
void iniWindow(int appData[],char windows[][appData[1]][appData[2]],int window);
void iniWindows(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2]);
void printWindowInfo(int appData[],int appCursors[][2]);
void printWindow(int appData[], char matrix[][appData[2]]);
void printCurrentWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]);
bool isLegalFile(int appData[],int y);
void setYCursorOnWindow(int appData[],int appCursors[][2],int y);
bool isLegalColumn(int appData[], int x);
void setXCursorOnWindow(int appData[],int appCursors[][2],int x);
//3.4
void insertCharOnWindow(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2],char c);
void deleteCurrentPositionOnWindow(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2]);

int main(){}
