//Samantha Roldán García
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "edth.h"

void iniEmptyArray(int columns, char array[]);
void printArray(int columns, char array[]);
void putCharOnArray(int columns, char array[], int column, char c);
void shiftRightArray(int columns, char array[], int column);
void shiftLeftArray(int columns, char array[], int column);
void insertCharOnArray(int columns, char array[], int column, char c);

int main(){}

