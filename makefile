all: arrays matrices ventanas main
.PHONY: all clean
arrays: arrays.o 
	gcc -o arrays arrays.o 
arrays.o: arrays.c
	gcc -c arrays.c
matrices: matrices.o
	gcc -o matrices matrices.o
matrices.o: matrices.c
	gcc -c matrices.c
ventanas: ventanas.o
	gcc -o ventanas ventanas.o
ventanas.o: ventanas.c
	gcc -c ventanas.c
main: main.o
	gcc -o main main.o
main.o: main.c
	gcc -c main.c
clean:
	rm *.o arrays matrices ventanas main
