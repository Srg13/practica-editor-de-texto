//Samantha Roldán García

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define INI ' '


void iniEmptyArray(int columns, char array[])
{
	for(int i = 0; i<=columns-1; i++)
	{
		array[i]= INI;
	}
}
void printArray(int columns, char array[])
{
	for(int i=0; i<=columns-1; i++)
	{
		printf("%c", array[i]);
	}
}
void putCharOnArray(int columns, char array[], int column, char c)
{
	//el put substituye un espacio en blanco por un carácter
	if(column < columns)
	{
		array[column]=c;
	}
}
void shiftRightArray(int columns, char array[], int column)
{
	int i= columns-1;
	while(i>column)
	{
		array[i]=array[i-1];
		i--;
	}
}
void shiftLeftArray(int columns, char array[], int column)
{
	int i=column;
	while(i<columns)
	{
		array[i]=array[i+1];
		i++;
	}
	array[columns-1] = INI;
	//tiene que borrar cuando desplaza
}
void insertCharOnArray(int columns, char array[], int column, char c)
{
	//el insert inserta un carácter y desplaza el texto a un lado
	shiftRightArray(columns,array,column);
	putCharOnArray(columns,array,column,c);
}

void iniEmptyMatrix(int files, int columns, char matrix[][15])
{
    for(int i=0; i<=files-1; i++)
	{
		iniEmptyArray(columns, matrix[i]);
	}
}
void printMatrix(int files, int columns, char matrix[][15])
{
	for (int i=0; i<=files-1; i++)
	{
		printf("%2i|",i);
		printArray(columns,matrix[i]);
		printf("|\n");
	}	
}
void insertCharOnMatrix(int files, int columns, char matrix[][15], int file, int column, char c)
{
	insertCharOnArray(columns,matrix[file],column,c);
}
void deleteCharOnMatrix(int files, int columns, char matrix[][15], int file, int column)
{
	//borrar un carácter de la matriz
	shiftLeftArray(columns,matrix[file],column);
}

void setActiveWindow(int appData[], int aw)
{
	appData[3]=aw;
}
void iniWindow(int appData[],char windows[][appData[1]][appData[2]],int window)
{
	iniEmptyMatrix(appData[1],appData[2],windows[window]);
}
void iniWindows(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2])
{
	iniEmptyMatrix(appData[1],appData[2],windows[appData[3]]);
}
void printWindowInfo(int appData[],int appCursors[][2])
{
	printf("%d -%d , %d",appData[3],appData[1],appData[2]);
}
void printWindow(int appData[], char matrix[][appData[2]])
{
	printf("\n");
	printMatrix(appData[1], appData[2], matrix);	
}
void printCurrentWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2])
{
	printWindowInfo(appData,appCursors);
	printWindow(appData,windows[appData[3]]);
}
bool isLegalFile(int appData[],int y)
{
	if(y<=appData[1])
	{
		return true;
	}
	return false;
}
void setYCursorOnWindow(int appData[],int appCursors[][2],int y)
{
	if(isLegalFile(appData,y))
	{
		appCursors[appData[3]][y];
	}
}
bool isLegalColumn(int appData[], int x)
{
	if(x<=appData[2])
	{
		return true;
	}
	return false;
}
void setXCursorOnWindow(int appData[],int appCursors[][2],int x)
{
	if(isLegalFile(appData,x))
	{
		appCursors[appData[3]][x];
	}
}

void insertCharOnWindow(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2],char c)
{
	insertCharOnMatrix(appData[1],appData[2],windows[appData[3]],appCursors[appData[3]][0],appCursors[appData[3]][1],c);
}
void deleteCurrentPositionOnWindow(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2])
{
	deleteCharOnMatrix(appData[1], appData[2],windows[appData[3]],appCursors[appData[3]][0], appCursors[appData[3]][1]);
}
